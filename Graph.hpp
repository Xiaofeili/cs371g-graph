// -------
// Graph.h
// --------

#ifndef Graph_h
#define Graph_h

// --------
// includes
// --------

#include <cassert> // assert
#include <cstddef> // size_t
#include <utility> // make_pair, pair
#include <vector>  // vector

// -----
// Graph
// -----

class Graph {
public:
    // ------
    // usings
    // ------

    using vertex_descriptor = int;
    using edge_descriptor   = std::pair<vertex_descriptor, vertex_descriptor>;

    using vertex_iterator    = std::set<vertex_descriptor>::iterator;
    using edge_iterator      = std::set<edge_descriptor>::iterator;
    using adjacency_iterator = std::set<vertex_descriptor>::iterator;;

    using vertices_size_type = std::size_t;
    using edges_size_type    = std::size_t;

public:
    // --------
    // add_edge
    // --------

    /**
     * add an edge
     * @param the source vertex
     * @param the target vertex
     * @param graph
     * @return a pair. The first element is the edge. The second element is a boolean value. It is true if the edge is added, is false when the edge already exists.
     */
    friend std::pair<edge_descriptor, bool> add_edge (vertex_descriptor v1, vertex_descriptor v2, Graph& g) {
        edge_descriptor ed = std::make_pair(v1, v2);
        bool            b  = g.e_set.insert(ed).second;
        if(b)
            g.adj[v1].insert(v2);
        return std::make_pair(ed, b);
    }

    // ----------
    // add_vertex
    // ----------

    /**
     * add a new vertex
     * @param graph
     * @return added vertex
     */
    friend vertex_descriptor add_vertex (Graph& g) {
        vertex_descriptor v = g.v_set.size();
        g.adj.push_back(std::set<vertex_descriptor>());
        g.v_set.insert(v);
        return v;
    }

    // -----------------
    // adjacent_vertices
    // -----------------

    /**
     * @param vertex
     * @param graph
     * @return an iterator-range providing access to the vertices adjacent to vertex u in graph g.
     */
    friend std::pair<adjacency_iterator, adjacency_iterator> adjacent_vertices (vertex_descriptor v, const Graph& g) {
        adjacency_iterator b = g.adj[v].begin();
        adjacency_iterator e = g.adj[v].end();
        return std::make_pair(b, e);
    }

    // ----
    // edge
    // ----

    /**
     * @param source vertex
     * @param target vertex
     * @param graph
     * @return If an edge from vertex u to vertex v exists, return a pair containing one such edge and true. If there are no edges between u and v, return a pair with an arbitrary edge descriptor and false.
     */
    friend std::pair<edge_descriptor, bool> edge (vertex_descriptor v1, vertex_descriptor v2, const Graph& g) {
        edge_descriptor ed = std::make_pair(v1, v2);
        bool            b  = g.e_set.count(ed);
        return std::make_pair(ed, b);
    }

    // -----
    // edges
    // -----

    /**
     * @param graph
     * @return an iterator-range providing access to the edge set of graph g.
     */
    friend std::pair<edge_iterator, edge_iterator> edges (const Graph& g) {
        edge_iterator b = g.e_set.begin();
        edge_iterator e = g.e_set.end();
        return std::make_pair(b, e);
    }

    // ---------
    // num_edges
    // ---------

    /**
     * @param graph
     * @return the number of edges in the graph g.
     */
    friend edges_size_type num_edges (const Graph& g) {
        return g.e_set.size();
    }

    // ------------
    // num_vertices
    // ------------

    /**
     * @param graph
     * @return the number of vertices in the graph g.
     */
    friend vertices_size_type num_vertices (const Graph& g) {
        return g.v_set.size();
    }

    // ------
    // source
    // ------

    /**
     * @param edge
     * @param graph
     * @return the source vertex of edge e.
     */
    friend vertex_descriptor source (edge_descriptor e, const Graph& g) {
        return e.first;
    }

    // ------
    // target
    // ------

    /**
     * @param edge
     * @param graph
     * @return the target vertex of edge e.
     */
    friend vertex_descriptor target (edge_descriptor e, const Graph& g) {
        return e.second;
    }

    // ------
    // vertex
    // ------

    /**
     * @param index
     * @param graph
     * @return the nth vertex in the graph's vertex list.
     */
    friend vertex_descriptor vertex (vertices_size_type v, const Graph& g) {
        return v;
    }

    // --------
    // vertices
    // --------

    /**
     * @param graph
     * @return an iterator-range providing access to the vertex set of graph g.
     */
    friend std::pair<vertex_iterator, vertex_iterator> vertices (const Graph& g) {
        vertex_iterator b = g.v_set.begin();
        vertex_iterator e = g.v_set.end();
        return std::make_pair(b, e);
    }

private:
    // ----
    // data
    // ----

    std::vector<std::set<vertex_descriptor>> adj;
    std::set<edge_descriptor> e_set;
    std::set<vertex_descriptor> v_set;

    // -----
    // valid
    // -----

    /**
     * @return if the graph is valid
     */
    bool valid () const {
        return adj.size() == v_set.size();
    }

public:
    // --------
    // defaults
    // --------

    Graph             ()             = default;
    Graph             (const Graph&) = default;
    ~Graph            ()             = default;
    Graph& operator = (const Graph&) = default;
};

#endif // Graph_h
